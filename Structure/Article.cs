﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Articles.Structures
{
    public class Article
    {
        protected string Designation { get; set; }
        protected int Prix { get; set; }
        protected int Quantite { get; set; }

        public void Afficher()
        {
            Console.WriteLine(this.Designation);
            Console.WriteLine(this.Prix);
            Console.WriteLine(this.Quantite);
        }

        public void Ajouter(int integer)
        {
            this.Prix = this.Prix + integer;
        }

        public void Retirer(int integer)
        {
            this.Prix = this.Prix - integer;
        }

        public Article(string designation, int prix, int quantite)
        {
            this.Designation = designation;
            this.Prix = prix;
            this.Quantite = quantite;
        }
    }
}
