﻿using System;
using Articles.Structures;

namespace Articles
{
    class Program
    {
        static void Main(string[] args)
        {
            var a1 = new Article("stp", 2, 2);

            Console.WriteLine("Entrez le nombre que vous voulez ajouter");
            a1.Ajouter(Int32.Parse(Console.ReadLine()));
            Console.WriteLine("Entrez le nombre que vous voulez retirer");
            a1.Retirer(Int32.Parse(Console.ReadLine()));
            a1.Afficher();
        }
    }
}
